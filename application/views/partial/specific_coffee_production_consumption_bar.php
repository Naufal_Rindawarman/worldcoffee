<div id="specific_production_consumption_growth"></div>

<script>        
            
    SPECIFICPRODUCTIONCONSUMPTION = document.getElementById("specific_production_consumption_growth"); 
    
    var id; //id negara;
    var name; //nama negara;
    
    var specificLayout = {
        barmode: 'relative',
        height: '400',        
        legend: {
            showLegend: true,
            x: 1,
            y: 1
          },
        xaxis: {
            title: 'Year',
            dtick: 1,
        },
        yaxis: {
            title: 'Quantity (in thousand 60kg bag)'
        }     
    }; 
    
    function setCountryProductionConsumptionbyID(c){
    	if (c != id){    	
    	    var cons3 = [];
            var prod3 = [];
            var cons3year = [];
            var prod3year = [];
            var specificData = [];
	    	for (var i = 0; i<countryData['features'].length; i++){
	    		var country = (countryData['features'][i]);	    		
	    		id = country['id']
	    		if (id == c){
	    		    //console.log(country['id']);
	    			country = country['properties'];
	    			var length = country['production'].length;
	    			//if(length>0){
	    			    name = country['name'];              
    		           	for (var j = 0; j<length; j++ ){                    
    		                var year = country['production'][j][0];
    		                var productionQuantity = country['production'][j][1];
    		                prod3year.push(year);
    		                prod3.push(productionQuantity);
    			        }
    			        specificData.push({
                            name: 'Production', 
                            x: prod3year, 
                            y: prod3, 
                            type: 'bar'
                        });
	    			//}
		           	
			        var length = country['consumption'].length;
			        //if(length>0){
			            for (var j = 0; j<length; j++ ){                    
                            year = country['consumption'][j][0];
                            var consumptionQuantity = -1*country['consumption'][j][1];
                            cons3year.push(year);
                            cons3.push(consumptionQuantity);
                        }
                        
                        specificData.push({
                            name: 'Consumption', 
                            x: cons3year, 
                            y: cons3, 
                            type: 'bar'
                        });
			        //}
                    specificLayout['title'] = name + "'s Coffee Production and Consumption";
	    			break;
	    		}
	    	}
	    	Plotly.newPlot(SPECIFICPRODUCTIONCONSUMPTION, specificData, specificLayout); 
    	}  	
         	
    }
    
</script>