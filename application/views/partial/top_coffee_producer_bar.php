<p>Brazil produced the most while United States consumed the most. Brazil also held the highest coffee production and consumption combined. </p>
<div id='top_producer_bar'></div>


<div class="col-md-12" style="text-align: center">
    <nav id='producer_year_selector' aria-label="Page navigation" style="text-align: center">
          <ul class="pagination">    
            <li><a href="#tester">2013</a></li>
            <li><a href="#tester">2014</a></li>
            <li><a href="#tester">2015</a></li>
            <li><a href="#tester">2016</a></li>
            <li class='active'><a href="#tester" >2017</a></li>    
          </ul>
        </nav>
    <form class="form-inline">
      <div class="form-group">
        <label for="minimumProductionTopBar">Minimum Production</label>
        <input type="number" class="form-control" id="minimumProductionTopBar" value=400 min=0>
      </div>
       <div class="form-group">
        <label for="sortSelectProductionTopBar">Sort</label>
        <select class="form-control" id="sortSelectProductionTopBar">
            <option value="production" selected>Production</option>
            <option value="consumption">Consumption</option>
        </select>
      </div>
      <div class="form-group">
        <label for="includeNonProducingTopBar">Include non-Producing Countries</label>
        <input type="checkbox" class="form-control" id="includeNonProducingTopBar">
      </div>
    </form>
</div>

<script>

    TOPPRODUCERBAR = document.getElementById('top_producer_bar');
    
    var prod_data = {
      x: [],
      y: [],
      name: 'Production',
      orientation: 'h',
      type: 'bar',
      marker: {
        color: 'rgba(55,128,191,0.6)',
        width: 1.2
      }
    };
    
    var cons_data = {
      x: [],
      y: [],
      name: 'Consumption',
      orientation: 'h',
      marker: {
        color: 'rgba(255,153,51,0.6)',
        width: 1.2
      },
      type: 'bar'
    };
    
    var minimumProductionTopBar = 400;
    var includeNonProducerProductionTopBar = false;  
    if (document.getElementById('includeNonProducingTopBar').checked){
        includeNonProducerProductionTopBar = true;
    }
    document.getElementById('minimumProductionTopBar').value = minimumProductionTopBar;
    var selectedYear = 2017; 
    var topProducerBarSort = "production";
    topProducerBarSort = document.getElementById('sortSelectProductionTopBar').value;
        
    
    
    function setTopProductionBarChart(){
        console.log('');
        var labels = [];
        
        var prod = [];
        var cons = [];
        
        if (selectedYear == null){
            selectedYear = 2017;
        }
        
        for (var i = 0; i<countryData['features'].length; i++){
            
            var country = (countryData['features'][i]['properties']);   
            //console.log(country);
            var productions_length = country['production'].length;
            if (productions_length < 1 && includeNonProducerProductionTopBar){
                //console.log("Nonproducing country");
                var consumption = 0;
                var name = country['name'];
                for (var j = 0; j<country['consumption'].length; j++){
                    var year = country['consumption'][j][0];
                    if (year == selectedYear){
                        consumption = -1*country['consumption'][j][1];
                        console.log(name + ", consumption: " + consumption);
                        labels.push(name);
                        cons.push(consumption);
                        prod.push(0);  
                    }
                }
            } else {
                for (var j = 0; j<productions_length; j++){
                    var year = country['production'][j][0];
                    if (year == selectedYear){
                        var production = country['production'][j][1];
                        if (production>=minimumProductionTopBar){
                            var name = country['name'];
                            //console.log(name);
                            labels.push(name);
                            prod.push(production);                            
                            var consumption = 0;
                            for (var k = 0; k<country['consumption'].length; k++){
                                if (country['consumption'][k][0] == selectedYear){
                                    consumption = country['consumption'][k][1];
                                    break;
                                }
                            }
                            cons.push(-1*consumption);
                        }
                    break;
                    }
                }
            }
            
        }   
        var sortedLabel = [];
        var sortedProd = [];
        var sortedCons = [];
        
        if (topProducerBarSort == "production"){
            var i = 0;
            while (prod.length>0){
                var minIdx = 0;
                var min = -99;
                i = 0;
                while (i<prod.length){
                    if (min == -99){
                        min = prod[i];
                        minIdx = i;
                        
                    } 
                    if (prod[i]<min){
                        min = prod[i];
                        minIdx = i;
                    }
                    i++;
                }
                sortedProd.push(prod[minIdx]);
                sortedCons.push(cons[minIdx]);
                sortedLabel.push(labels[minIdx]);
                prod.splice(minIdx, 1);
                cons.splice(minIdx, 1);
                labels.splice(minIdx, 1);
            }
        } else {
            var i = 0;
            while (cons.length>0){
                var minIdx = 0;
                var min = -99;
                i = 0;
                while (i<cons.length){
                    if (min == -99){
                        min = cons[i]*-1;
                        minIdx = i;
                        
                    } 
                    if (cons[i]*(-1)<min){
                        min = cons[i]*-1;
                        minIdx = i;
                    }
                    i++;
                }
                
                sortedProd.push(prod[minIdx]);
                sortedCons.push(cons[minIdx]);
                sortedLabel.push(labels[minIdx]);
                prod.splice(minIdx, 1);
                cons.splice(minIdx, 1);
                labels.splice(minIdx, 1);
            }
        }        
        
        prod_data.x = sortedProd;
        prod_data.y = sortedLabel;
        cons_data.x = sortedCons;
        cons_data.y = sortedLabel;
        
        //console.log(sortedProd);
        
        var data = [prod_data, cons_data];
        
        var layout = {
            barmode: 'relative',
            height: '800',
            legend: {
                x: 100,
                y: 1
              },
              xaxis: {
                title: 'Quantity (in thousand 60kg bag)'
              }      
        }; 
        
        Plotly.newPlot(TOPPRODUCERBAR, data, layout);    
    }    
    
    
    
    
    setTopProductionBarChart();
    
    
       

</script>

<script>
    $(document).ready(function(){
        $("#producer_year_selector li").click(function(){
            var val = $(this).children('a:first').text();
            $("#producer_year_selector li").removeClass('active');
            $(this).toggleClass('active');
            selectedYear = val;
            setTopProductionBarChart();        
        });
        
        $("#includeNonProducingTopBar").change(function(e){
            if ($(this).attr('checked')){
                $(this).removeAttr('checked');
                includeNonProducerProductionTopBar = false;
            } else {
                $(this).attr('checked', 'checked');
                includeNonProducerProductionTopBar = true;
            }
            setTopProductionBarChart(); 
        });
        
        $("#sortSelectProductionTopBar").change(function(e){
            topProducerBarSort = $(this).val(); 
            setTopProductionBarChart();
        });
        
        $("#minimumProductionTopBar").change(function(){
            minimumProductionTopBar = $(this).val();
            setTopProductionBarChart(); 
        });
        
        $(window).resize(function(){
            setTopProductionBarChart(); 
        })
    });
</script>