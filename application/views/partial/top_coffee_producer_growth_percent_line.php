<div id='top_producer_growth_percent_line'></div>



<script>        
            
    TOPPRODUCERGROWTHLINE = document.getElementById('top_producer_growth_percent_line');
    var minimumProductionTopGrowthLine = 5000;
    document.getElementById('minimumProductionTopGrowthLine').value = minimumProductionTopGrowthLine;
        
    function setTopProductionLineGrowthChart(){
        var topproducer = [];
        var topproduceryear = [];
        var topproducerlinedata = [];   
        
        var topproducerlinelayout = {
            height: '600',
            legend: {
                x: 100,
                y: 1
              },
              xaxis: {
                title: 'Year',
                dtick: 1,
              },
              yaxis: {
                title: 'Quantity (in thousand 60kg bag)'
              }      
        };
        
        var insertData = true;
        var previousQuantity;
        for (var i = 0; i<countryData['features'].length; i++){
            topproducer = [];
            topproduceryear = [];
            insertData = true;
            var previousQuantity;
            var country = (countryData['features'][i]['properties']);
            var length = country['production'].length;        
            if (length>2){  
               var productioninyear = [];
               var name = country['name'];              
               for (var j = 0; j<length; j++ ){ 
                    var productionQuantity = country['production'][j][1];
                    
                    if (productionQuantity<minimumProductionTopLine){
                        insertData = false;
                        break;
                    }     
                    if (j == 0){
                        previousQuantity = productionQuantity;
                        continue;
                    }        
                             
                    var year = country['production'][j][0];
                    topproduceryear.push(year);
                    topproducer.push((productionQuantity-previousQuantity)/previousQuantity);
                    //cons.push(-1*(production-300));                    
                    
               }         
                if (insertData){
                    topproducerlinedata.push({
                        name: name, 
                        x: topproduceryear, 
                        y: topproducer, 
                        type: 'scatter'
                    });
                }
            }
        }  
        
        
        
        Plotly.newPlot(TOPPRODUCERGROWTHLINE, topproducerlinedata, topproducerlinelayout);
    }
    
    setTopProductionLineGrowthChart();   
    
</script>

<script>
    $(document).ready(function(){
    $("#minimumProductionTopGrowthLine").change(function(){
        minimumProductionTopGrowthLine = $(this).val();
        setTopProductionLineGrowthChart(); 
    });
});
</script>