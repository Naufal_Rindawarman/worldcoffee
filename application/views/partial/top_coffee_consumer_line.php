<div class="col-md-8" >
    <div id='top_consumption_line'></div>
</div>
<div class="col-md-4" id=''>
    <p>Despite not being a coffee producer, United States is the largest coffee consumer for the last five years with over 23 thousand 60 kg bag. Followed by Brazil, a major coffee producer, in second place with 20 thousand 60 kg bag. Japan took the third place with a huge gap at 7.5 thousand 60 kg bag.</p>
</div>

<div class="col-md-12" style="text-align: center">
    <form class="form-inline">
      <div class="form-group">
        <label for="minimumConsumptionTopLine">Minimum Consumption</label>
        <input type="number" class="form-control" id="minimumConsumptionTopLine" value=400 min=0>
      </div>
    </form>
</div>

<script>        
            
    TOPCONSUMERLINE = document.getElementById('top_consumption_line');
    var minimumConsumptionTopLine = 400;
    document.getElementById('minimumConsumptionTopLine').value = minimumConsumptionTopLine;
        
    function setTopConsumptionLineChart(){
        var topconsumer = [];
        var topconsumeryear = [];
        var topconsumerlinedata = [];   
        
        var topconsumerlinelayout = {
            height: '600',
            legend: {
                x: 100,
                y: 1
              },
              xaxis: {
                title: 'Year',
                dtick: 1,
              },
              yaxis: {
                title: 'Quantity (in thousand 60kg bag)'
              }      
        };
        
        var insertData = true;
        for (var i = 0; i<countryData['features'].length; i++){
            topconsumer = [];
            topconsumeryear = [];
            insertData = true;
            var country = (countryData['features'][i]['properties']);
            var length = country['consumption'].length;        
            if (length>2){  
               var consumptioninyear = [];
               var name = country['name'];              
               for (var j = 0; j<length; j++ ){ 
                    var consumptionQuantity = country['consumption'][j][1];
                    if (consumptionQuantity<minimumConsumptionTopLine){
                        insertData = false;
                        break;
                    }                      
                    var year = country['consumption'][j][0];
                    topconsumeryear.push(year);
                    topconsumer.push(consumptionQuantity);
                    
               }         
                if (insertData){
                    topconsumerlinedata.push({
                        name: name, 
                        x: topconsumeryear, 
                        y: topconsumer, 
                        type: 'scatter'
                    });
                }
            }
        }  
        
        
        
        Plotly.newPlot(TOPCONSUMERLINE, topconsumerlinedata, topconsumerlinelayout);
    }
    
    setTopConsumptionLineChart();   
    
</script>

<script>
    $(document).ready(function(){
        $("#minimumConsumptionTopLine").change(function(){
            minimumConsumptionTopLine = $(this).val();
            setTopConsumptionLineChart(); 
        });
        
        $(window).resize(function(){
            setTopConsumptionLineChart(); 
        });
    });
</script>