<canvas id="productionshare" width="500"></canvas>

<script>
	var pie1 = document.getElementById("productionshare"); 
	
	var colorspallete = [
	                'rgba(255, 99, 132, 0.2)'
	                /*'rgba(54, 162, 235, 0.2)',
	                'rgba(255, 206, 86, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(153, 102, 255, 0.2)',
	                'rgba(255, 159, 64, 0.2)'*/
	            ];
	var labels = [];
	var prod = [];
	var cons = [];
	
	var colors = [];
	var colors2 = [];
	var colorIdx = 0;
	
	for (var i = 0; i<countryData['features'].length; i++){
	    var country = (countryData['features'][i]['properties']);            
	    var production = country['production'][country['production'].length-1][1];
	    if (production>400){
	        var name = country['name'];
	        labels.push(name);
	        prod.push(production);                
	        if (colorIdx == colorspallete.length){
	            colorIdx = 0;
	        }
	        colors.push(colorspallete[colorIdx]);
	        colors2.push(colorspallete[colorIdx+1]);
	        colorIdx++;
	        
	        console.log(name + ", consumption: " + country['consumption'].length);
	        if (country['consumption'].length > 0){
	            cons.push(-1*country['consumption'][country['consumption'].length-1][1]);
	        } else {
	            cons.push(0);
	        }
	    }
	    
	}
	    var pieChart = new Chart(pie1,{
	        type: 'pie',
	        data: {
	        datasets: [{
	            data: prod,
	            label: 'Dataset 1',
	            backgroundColor: colors
	        }],
	        labels: labels
	    }
	        
    });
</script>