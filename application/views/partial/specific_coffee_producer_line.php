<div id="specific_growth_line"></div>

<script>        
            
    SPECIFICPRODUCERLINE = document.getElementById("specific_growth_line"); 
    
    var labels3 = [];
    var values3 = [];
    
    var specificproductionlinedata = [];
    
    var id; //id negara;
    var name; //nama negara;
    
    var specificproductionlinelayout = {
        height: 500,
        xaxis: {
            title: 'Year',
            dtick: 1,
        },
        yaxis: {
            title: 'Quantity (in thousand 60kg bag)'
        }          
    }; 
    
    function setCountryProductionbyID(c){
    	if (c != id){    	
    	    specificproductionlinedata = [];
    	    labels3 = [];
            values3 = [];
	    	for (var i = 0; i<countryData['features'].length; i++){
	    		var country = (countryData['features'][i]);	    		
	    		id = country['id']
	    		if (id == c){
	    		    console.log(country['id']);
	    			country = country['properties'];
	    			var length = country['production'].length;
		           	name = country['name'];              
		           	for (var j = 0; j<length; j++ ){                    
		                var year = country['production'][j][0];
		                var productionQuantity = country['production'][j][1];
		                labels3.push(year);
		                values3.push(productionQuantity);
		                //console.log(year + ", " + productionQuantity);           
			        }
	                //specificlinedata[0]['values'] = values3;
                    //specificlinedata[0]['labels'] = labels3;
                    //specificlinedata[0]['name'] = name;
                    specificproductionlinedata.push({
                        name: name, 
                        x: labels3, 
                        y: values3, 
                        type: 'scatter'
                    });  
                    specificproductionlinelayout['title'] = name + "'s Coffe Production";
	    			break;
	    		}
	    	}
    	}  	
    	
        
        Plotly.newPlot(SPECIFICPRODUCERLINE, specificproductionlinedata, specificproductionlinelayout);  	
    }
    
</script>