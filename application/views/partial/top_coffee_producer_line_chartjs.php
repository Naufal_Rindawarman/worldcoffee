<canvas id="growth" width="500"></canvas>

<script>        
            
    var line1 = document.getElementById("growth"); 
    //var prod2 = [];
    //var cons2 = [];
    
    var dataset = [];
    
    var colorspallete2 = [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ];
    var colorIdx2 = 0;
    
    for (var i = 0; i<countryData['features'].length; i++){
        var country = (countryData['features'][i]['properties']);
        var length = country['production'].length;
        console.log(length);
        if (length>2){  
           var productioninyear = [];
           var name = country['name'];              
           for (var j = 0; j<length; j++ ){                    
                var year = country['production'][j][0];
                var productionQuantity = country['production'][j][1];
                productioninyear.push({x: year, y: productionQuantity});
                //labels.push(name);
                //prod.push(production);
                //cons.push(-1*(production-300));                    
                
           }
           if (colorIdx2 == colorspallete2.length){
                colorIdx2 = 0;
            }
            colorIdx2++;
           	dataset.push({'label': name, 'data': productioninyear, 'fill':false, 'borderColor': colorspallete2[colorIdx2], 'backgroundColor': colorspallete2[colorIdx2]}); 
        }
    }
    
    var lineChart = new Chart(line1, {
        type: 'scatter',
        data: {
            //labels: [2013, 2014,2015,2016, 2017],
            datasets: dataset
        },
        options: {
            showLines: true, // disable for all datasets
            scales: {
                xAxes: [{                        
                            stepSize: 1,
                            time: {
                                unit: 'year'
                            }
                        }
                        ]        
            },               
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            },
            title: {
                display: true,
                text: 'World Coffee Production Growth '
            }
        }
    });
</script>