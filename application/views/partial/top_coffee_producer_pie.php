<div class="col-md-8" id=''>
    <div id='top_producer_pie'></div>
</div>

<div class="col-md-4" id=''>
    <p>Being first, Brazil produces more than quarter of world coffee production. Followed by Vietnam with about eighth of world coffee production</p>
</div>

<div class="col-md-12" style="text-align: center">
    <nav id='producer_pie_year_selector' aria-label="Page navigation" style="text-align: center">
          <ul class="pagination">    
            <li><a href="#top_producer_pie">2013</a></li>
            <li><a href="#top_producer_pie">2014</a></li>
            <li><a href="#top_producer_pie">2015</a></li>
            <li><a href="#top_producer_pie">2016</a></li>
            <li class='active'><a href="#top_producer_pie" >2017</a></li>    
          </ul>
        </nav>
    <form class="form-inline">
      <div class="form-group">
        <label for="minimumProductionTopPie">Minimum Production</label>
        <input type="number" class="form-control" id="minimumProductionTopPie" value=400 min=0>
      </div>
    </form>
</div>

<script>
	TOPPRODUCERPIE = document.getElementById('top_producer_pie');
	
	
	var minimumProductionTopPie = 400; 
    document.getElementById('minimumProductionTopPie').value = minimumProductionTopPie;
    
    var topPieSelectedYear = 2017;
	
	function setTopProductionPieChart(){
	    var piedata = [{
          values: [],
          labels: [],
          type: 'pie'
        }];
	    var otherValue = 0;
	    
	    
	    for (var i = 0; i<countryData['features'].length; i++){
            var country = (countryData['features'][i]['properties']);
            var length = country['production'].length;
            for (var j = 0; j<length;j++){
                if (country['production'][j][0] != topPieSelectedYear){
                    continue;
                }
                var production = country['production'][j][1];
                if (production>=minimumProductionTopPie){
                    var name = country['name'];
                    piedata[0]['labels'].push(name);
                    piedata[0]['values'].push(production);
                } else {
                    otherValue+=production;
                }
                break;
            }
                
        }
        
        if (otherValue>0){
            piedata[0]['labels'].push('Other');
            piedata[0]['values'].push(otherValue);
            console.log(otherValue);
        }
        
        var layout = {
          height: 800
        };
    
        Plotly.newPlot(TOPPRODUCERPIE, piedata, layout);
	}
	
	setTopProductionPieChart();

</script>

<script>
    $(document).ready(function(){
        
        $("#producer_pie_year_selector li").click(function(){
            var val = $(this).children('a:first').text();
            $("#producer_pie_year_selector li").removeClass('active');
            $(this).toggleClass('active');
            topPieSelectedYear = val;
            setTopProductionPieChart();        
        });
        
        
        $("#minimumProductionTopPie").change(function(){
            minimumProductionTopPie = $(this).val();
            setTopProductionPieChart(); 
        });
        
        $(window).resize(function(){
            setTopProductionPieChart(); 
        });
    });
</script>