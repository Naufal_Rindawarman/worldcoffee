<p>Countries with zero production-consumption ratio have equal production and consumption. Countries with positive ratio have production surplus. Meanwhile countries with negative ratio consume more than it can produce.
    Philippines has extreme cases of disparity between coffee produced and consumed.    
</p>
<div id='top_producer_ratio_bar'></div>
<div class="col-md-12" style="text-align: center">
    <nav id='producer_ratio_year_selector' aria-label="Page navigation" style="text-align: center">
          <ul class="pagination">    
            <li><a href="#tester">2013</a></li>
            <li><a href="#tester">2014</a></li>
            <li><a href="#tester">2015</a></li>
            <li><a href="#tester">2016</a></li>
            <li class='active'><a href="#tester" >2017</a></li>    
          </ul>
        </nav>
    <form class="form-inline">
      <div class="form-group">
        <label for="minimumProductionConsumptionRatioBar">Minimum Production</label>
        <input type="number" class="form-control" id="minimumProductionConsumptionRatioBar" value=100 min=0>
      </div>
    </form>
</div>

<script>

    TOPPRODUCERCONSUMPTIONRATIOBAR = document.getElementById('top_producer_ratio_bar');
    
    
    var minimumProductionConsumptionRatioBar = 100;
    document.getElementById('minimumProductionConsumptionRatioBar').value = minimumProductionConsumptionRatioBar;
    var selectedYear = 2017; 
    
    function setTopProductionConsumptionRatioBarChart(){
        
        var ratio_data = {
          x: [],
          y: [],
          name: 'Ratio',
          orientation: 'h',
          type: 'bar',
          marker: {
            color: 'rgba(55,128,191,0.6)',
            width: 1.2
          }
        };
        
        var labels = [];
        var ratios = [];
        
        if (selectedYear == null){
            selectedYear = 2017;
        }
        
        for (var i = 0; i<countryData['features'].length; i++){
            var country = (countryData['features'][i]['properties']);   
            //console.log(country);
            var productions_length = country['production'].length;
            if (productions_length > 0){
                for (var j = 0; j<productions_length; j++){
                    var year = country['production'][j][0];
                    if (year == selectedYear){
                        var production = country['production'][j][1];
                        if (production>=minimumProductionConsumptionRatioBar && production>0){
                            var name = country['name'];
                            //console.log(name);
                            for (var k = 0; k<country['consumption'].length; k++){
                                if (country['consumption'][k][0] == selectedYear){
                                    var consumption = country['consumption'][k][1];
                                    if (consumption > 0){
                                        labels.push(name);
                                        ratios.push(100*(((production-consumption)/production)));
                                    }
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }   
        var sortedLabels = [];
        var sortedRatios = [];
        
        
        var i = 0;
        while (ratios.length>0){
            var minIdx = 0;
            var min = -99;
            i = 0;
            while (i<ratios.length){
                if (min == -99){
                    min = ratios[i];
                    minIdx = i;
                    
                } 
                if (ratios[i]<min){
                    min = ratios[i];
                    minIdx = i;
                }
                i++;
            }
            sortedRatios.push(ratios[minIdx]);
            sortedLabels.push(labels[minIdx]);
            ratios.splice(minIdx, 1);
            labels.splice(minIdx, 1);
        }
        
        ratio_data.x = sortedRatios;
        ratio_data.y = sortedLabels;
        
        var data = [ratio_data];
        
        
       
       var layout = {
            barmode: 'relative',
            height: '800',
            legend: {
                x: 100,
                y: 1
              },
              xaxis: {
                title: 'Ratio (in percent)'
              }      
        }; 
       
        //ratio_data.x = ratios;
        //ratio_data.y = labels;
        //var data = [ratio_data];
        
        Plotly.newPlot(TOPPRODUCERCONSUMPTIONRATIOBAR, data, layout);    
    }    
    
    setTopProductionConsumptionRatioBarChart();

</script>

<script>
    $(document).ready(function(){
        $("#producer_ratio_year_selector li").click(function(){
            var val = $(this).children('a:first').text();
            $("#producer_ratio_year_selector li").removeClass('active');
            $(this).toggleClass('active');
            selectedYear = val;
            setTopProductionConsumptionRatioBarChart();        
        });
        
        $("#sortSelectProductionConsumptionRatioBar").change(function(e){
            topProducerRatioBarSort = $(this).val(); 
            setTopProductionConsumptionRatioBarChart();
        });
        
        $("#minimumProductionConsumptionRatioBar").change(function(){
            minimumProductionConsumptionRatioBar = $(this).val();
            setTopProductionConsumptionRatioBarChart(); 
        });
        
        $(window).resize(function(){
            setTopProductionConsumptionRatioBarChart(); 
        })
    });
</script>