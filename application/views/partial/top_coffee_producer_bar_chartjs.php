<canvas id="bar" width="500"></canvas>

<script>
	var bar1 = document.getElementById("bar"); 
	
	var colorspallete = [
                        'rgba(255, 99, 132, 0.2)'
                        /*'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'*/
                    ];
    var labels = [];
    var prod = [];
    var cons = [];
    
    var colors = [];
    var colors2 = [];
    var colorIdx = 0;
    
    for (var i = 0; i<countryData['features'].length; i++){
        var country = (countryData['features'][i]['properties']);            
        var production = country['production'][country['production'].length-1][1];
        if (production>400){
            var name = country['name'];
            labels.push(name);
            prod.push(production);                
            if (colorIdx == colorspallete.length){
                colorIdx = 0;
            }
            colors.push(colorspallete[colorIdx]);
            colors2.push(colorspallete[colorIdx+1]);
            colorIdx++;
            
            console.log(name + ", consumption: " + country['consumption'].length);
            if (country['consumption'].length > 0){
                cons.push(-1*country['consumption'][country['consumption'].length-1][1]);
            } else {
                cons.push(0);
            }
        }
        
    }
           
    var barChart = new Chart(bar1, {
        type: 'horizontalBar',            
        data: {
            //labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
            labels: labels,
            datasets: [{
                label: 'Coffee Consumption',
                data: cons,
                /*backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],*/
                backgroundColor: colors2,
                borderWidth: 1
                },
                {
                    label: 'Coffee Production',
                    data: prod,
                    /*backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],*/
                    backgroundColor: colors,
                    borderWidth: 1
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero:true,
                        callback: function(value, index, values) {
                            if (value<0){
                               return -1*value; 
                            } else {
                               return value;
                            }
                            
                        }
                    }
                }],
                yAxes: [{
                    stacked: true,
                    
                    ticks: {
                        beginAtZero:true
                        
                    }
                }]
            },
            title: {
                display: true,
                text: 'World Coffee Production and Consumption '
            }
        }
    });
</script>