

<div class="col-md-8" id=''>
    <div id='top_producer_line'></div>
</div>

<div class="col-md-4" id=''>
    <p>Vietnam had a significant increase in production from 2013-2016 albeit decreasing below 2015's output in 2017. Colombia had a steady increase in production in 2013-2017. Meanwhile, world's number one coffee producer Brazil was stagnating in recent years.</p>
</div>

<div class="col-md-12" style="text-align: center">
    <form class="form-inline">
      <div class="form-group">
        <label for="minimumProductionTopLine">Minimum Production</label>
        <input type="number" class="form-control" id="minimumProductionTopLine" value=400 min=0>
      </div>
    </form>
</div>

<script>        
            
    TOPPRODUCERLINE = document.getElementById('top_producer_line');
    var minimumProductionTopLine = 400;
    document.getElementById('minimumProductionTopLine').value = minimumProductionTopLine;
        
    function setTopProductionLineChart(){
        var topproducer = [];
        var topproduceryear = [];
        var topproducerlinedata = [];   
        
        var topproducerlinelayout = {
            height: '600',
            legend: {
                x: 100,
                y: 1
              },
              xaxis: {
                title: 'Year',
                dtick: 1,
              },
              yaxis: {
                title: 'Quantity (in thousand 60kg bag)'
              }      
        };
        
        var insertData = true;
        for (var i = 0; i<countryData['features'].length; i++){
            topproducer = [];
            topproduceryear = [];
            insertData = true;
            var country = (countryData['features'][i]['properties']);
            var length = country['production'].length;        
            if (length>2){  
               var productioninyear = [];
               var name = country['name'];              
               for (var j = 0; j<length; j++ ){ 
                    var productionQuantity = country['production'][j][1];
                    if (productionQuantity<minimumProductionTopLine){
                        insertData = false;
                        break;
                    }                      
                    var year = country['production'][j][0];
                    topproduceryear.push(year);
                    topproducer.push(productionQuantity);
                    //cons.push(-1*(production-300));                    
                    
               }         
                if (insertData){
                    topproducerlinedata.push({
                        name: name, 
                        x: topproduceryear, 
                        y: topproducer, 
                        type: 'scatter'
                    });
                }
            }
        }  
        
        
        
        Plotly.newPlot(TOPPRODUCERLINE, topproducerlinedata, topproducerlinelayout);
    }
    
    setTopProductionLineChart();   
    
</script>

<script>
    $(document).ready(function(){
        $("#minimumProductionTopLine").change(function(){
            minimumProductionTopLine = $(this).val();
            setTopProductionLineChart(); 
        });
        
        $(window).resize(function(){
            setTopProductionLineChart(); 
        });
    });
</script>