<div id="specific_consumption_line"></div>

<script>        
            
    SPECIFICCONSUMPTIONLINE = document.getElementById("specific_consumption_line"); 
    
    var labels4 = [];
    var values4 = [];
    
    var specificconsumptionlinedata = [];
    
    var id4; //id negara;
    var name4; //nama negara;
    
    var specificconsumptionlinelayout = {
        height: 500,
        xaxis: {
            title: 'Year',
            dtick: 1,
        },
        yaxis: {
            title: 'Quantity (in thousand 60kg bag)'
        }          
    }; 
    
    function setCountryConsumptionbyID(c){
    	if (c != id4){    	
    	    specificconsumptionlinedata = [];
    	    labels4 = [];
            values4 = [];
	    	for (var i = 0; i<countryData['features'].length; i++){
	    		var country = (countryData['features'][i]);	    		
	    		id = country['id']
	    		if (id == c){
	    		    console.log(country['id']);
	    			country = country['properties'];
	    			var length = country['consumption'].length;
		           	name4 = country['name'];              
		           	for (var j = 0; j<length; j++ ){                    
		                var year = country['consumption'][j][0];
		                var quantity = country['consumption'][j][1];
		                labels4.push(year);
		                values4.push(quantity);
		                //console.log(year + ", " + productionQuantity);           
			        }
	                //specificlinedata[0]['values'] = values3;
                    //specificlinedata[0]['labels'] = labels3;
                    //specificlinedata[0]['name'] = name;
                    specificconsumptionlinedata.push({
                        name: name4, 
                        x: labels4, 
                        y: values4, 
                        type: 'scatter'
                    });  
                    specificconsumptionlinelayout['title'] = name4 + "'s Coffe Consumption";
	    			break;
	    		}
	    	}
    	}  	
    	
        
        Plotly.newPlot(SPECIFICCONSUMPTIONLINE, specificconsumptionlinedata, specificconsumptionlinelayout);  	
    }
    
</script>