<canvas id="specific_growth" width="500"></canvas>

<script>        
            
    var specificline = document.getElementById("specific_growth"); 
    
    var specificLineChart;
    //var prod2 = [];
    //var cons2 = [];
    
    var dataset_specific = [];
    
    var colorspallete2 = [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ];
    var colorIdx2 = 0;
    
    var id //id negara;
    var name //nama negara;
    
    function setCountrybyID(c){
    	if (c != id){
    		colorIdx2 = 0;
	    	dataset_specific = [];
	    	for (var i = 0; i<countryData['features'].length; i++){
	    		var country = (countryData['features'][i]);
	    		//console.log(country['id']);
	    		id = country['id']
	    		if ( id == c){
	    			country = country['properties'];
	    			var length = country['production'].length;
					var productioninyear = [];
		           	name = country['name'];              
		           	for (var j = 0; j<length; j++ ){                    
		                var year = country['production'][j][0];
		                var productionQuantity = country['production'][j][1];
		                productioninyear.push({x: year, y: productionQuantity});
		                //labels.push(name);
		                //prod.push(production);
		                //cons.push(-1*(production-300));                    
			                
			        }
		           	if (colorIdx2 == colorspallete2.length){
			                colorIdx2 = 0;
			        }
			        colorIdx2++;
			        dataset_specific.push({'label': name, 'data': productioninyear, 'fill':false, 'borderColor': colorspallete2[colorIdx2], 'backgroundColor': colorspallete2[colorIdx2]}); 
		        
	    			break;
	    		}
	    		console.log('success');
	    	}
	    	
	    	if (specificLineChart  == null){
	    		specificLineChart = newSpecificLineChart();
	    	} else {
	    		specificLineChart.data.datasets = dataset_specific;
	    		specificLineChart.update();
	    	}
    	}    	
    }
    
    function newSpecificLineChart(){
    	return new Chart(specificline, {
	        type: 'scatter',
	        data: {            
	            datasets: dataset_specific
	        },
	        options: {
	            showLines: true, // disable for all datasets
	            scales: {
	                xAxes: [{                        
	                            stepSize: 1
	                        }
	                        ]        
	            },               
	            elements: {
	                line: {
	                    tension: 0, // disables bezier curves
	                }
	            },
	            title: {
	                display: true,
	                text: name + ' Coffee Production Growth '
	            }
	        }
	    });
    }       
    
    
    
</script>