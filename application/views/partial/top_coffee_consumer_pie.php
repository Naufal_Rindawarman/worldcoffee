
<div class="col-md-8" id=''>
    <div id='top_consumer_pie'></div>
</div>

<div class="col-md-4" id=''>
    <p>United States contributed about quarter of world's coffee consumption in 2013-2016. Brazil took the second place with share a bit over a fifth.</p>
</div>

<div class="col-md-12" style="text-align: center">
    <nav id='consumer_pie_year_selector' aria-label="Page navigation" style="text-align: center">
          <ul class="pagination">    
            <li><a href="#top_consumer_pie">2013</a></li>
            <li><a href="#top_consumer_pie">2014</a></li>
            <li><a href="#top_consumer_pie">2015</a></li>
            <li class='active'><a href="#top_consumer_pie">2016</a></li>
            <li ><a href="#top_consumer_pie" >2017</a></li>    
          </ul>
        </nav>
    <form class="form-inline">
      <div class="form-group">
        <label for="minimumConsumptionTopPie">Minimum Consumption</label>
        <input type="number" class="form-control" id="minimumConsumptionTopPie" value=400 min=0>
      </div>
    </form>
</div>

<script>
	TOPCONSUMERPIE = document.getElementById('top_consumer_pie');
	
	
	var minimumConsumerTopPie = 400; 
    document.getElementById('minimumConsumptionTopPie').value = minimumConsumerTopPie;
    
    var topConsumptionPieSelectedYear = 2016;
	
	function setTopConsumptionPieChart(){
	    var piedata = [{
          values: [],
          labels: [],
          type: 'pie'
        }];
	    var otherValue = 0;
	    
	    
	    for (var i = 0; i<countryData['features'].length; i++){
            var country = (countryData['features'][i]['properties']);
            var length = country['consumption'].length;
            for (var j = 0; j<length;j++){
                if (country['consumption'][j][0] != topConsumptionPieSelectedYear){
                    continue;
                }
                var consumption = country['consumption'][j][1];
                if (consumption>=minimumConsumerTopPie){
                    var name = country['name'];
                    piedata[0]['labels'].push(name);
                    piedata[0]['values'].push(consumption);
                } else {
                    otherValue+=consumption;
                }
                break;
            }
                
        }
        
        if (otherValue>0){
            piedata[0]['labels'].push('Other');
            piedata[0]['values'].push(otherValue);
            console.log(otherValue);
        }
        
        var layout = {
          height: 800
        };
    
        Plotly.newPlot(TOPCONSUMERPIE, piedata, layout);
	}
	
	setTopConsumptionPieChart();

</script>

<script>
    $(document).ready(function(){
        
        $("#consumer_pie_year_selector li").click(function(){
            var val = $(this).children('a:first').text();
            $("#consumer_pie_year_selector li").removeClass('active');
            $(this).toggleClass('active');
            topConsumptionPieSelectedYear = val;
            setTopConsumptionPieChart();        
        });
        
        
        $("#minimumConsumptionTopPie").change(function(){
            minimumConsumerTopPie = $(this).val();
            setTopConsumptionPieChart(); 
        });
        
        $(window).resize(function(){
            setTopConsumptionPieChart(); 
        });
    });
</script>