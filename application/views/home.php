<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>World's Coffee Production and Consumption</title>
        
        <script src="<?php echo base_url('/js/jquery/jquery.min.js');?>"></script>
        <script src="<?php echo base_url('/js/bootstrap/bootstrap.min.js');?>"></script>        
        <link rel="stylesheet" href="<?php echo base_url('/css/bootstrap/bootstrap.min.css');?>">        
        
        
        
        
        <!---Leaflet-->
        <script src="<?php echo base_url().'thirdparty/leaflet/leaflet.js';?>"></script>
        <script src="<?php echo base_url().'thirdparty/leaflet/leaflet-src.js';?>"></script>
        <link rel="stylesheet" href="<?php echo base_url().'thirdparty/leaflet/leaflet.css';?>">
        
        <!---Geo Data-->
        <script src="<?php echo base_url().'js/coffeedata/major.producer.countries.coffee.js';?>"></script>
        <script src="<?php echo base_url().'js/coffeedata/major.consumer.countries.coffee.js';?>"></script>
        
        <!--Data-->
        <script src="<?php echo base_url().'js/coffeedata/worldpopulation.js';?>"></script>
        <script src="<?php echo base_url().'js/coffeedata/worldarea.js';?>"></script>
        <script src="<?php echo base_url().'js/coffeedata/worldnominalgdppercapita.js';?>"></script>
        <script src="<?php echo base_url().'js/coffeedata/countrydescription.js';?>"></script>
        
        <!--ChartJS-->
        <script src="<?php echo base_url().'thirdparty/chartjs/Chart.min.js';?>"></script>
        <script src="<?php echo base_url().'thirdparty/chartjs/Chart.bundle.min.js';?>"></script>
        
        <!--Plot.ly-->
        <script src="<?php echo base_url().'thirdparty/plotly/plotly.min.js';?>"></script>
        
        <!--Flag-sprites-->
        <link rel="stylesheet" href="<?php echo base_url('thirdparty/flagsprites/flags.edited.css');?>">
        
        <link rel="stylesheet" href="<?php echo base_url('css/style.css');?>">
        
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="jumbotron" id="jumbo">
                    <h1 style="font-size: 32pt">World's Coffee Production and Consumption</h1>
                    <p>Which country produces most coffee and drinks most coffee. Let's find out.</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id='map' style='height: 512px;'></div>
                    
                </div>   
                <div class="col-md-12">
                	<div class="well" id="select-country-prompt">Select any highlighted country from the map to view details</div>
                	<div class="panel panel-default" id="country-detail" style="display:none;">
				  		<div class="panel-body">
						  	<div class="row">
						  		<div class="col-md-6">
				                	<span style="display:inline;" id='country-detail-flag'></span>
				                	<h2 style="display:inline;" id="country-detail-title">Brazil</h2>	
				                	<hr>
				                	<form class="form-horizontal">
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Population</label>
                                        <div class="col-sm-9">
                                          <p class="form-control-static" id="country-detail-population"></p>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Area</label>
                                        <div class="col-sm-9">
                                          <p class="form-control-static"id="country-detail-area"></p>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Nominal GDP per-Capita</label>
                                        <div class="col-sm-9">
                                          <p class="form-control-static" id="country-detail-nominalgdppercapita"></p>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Coffee Cultivar</label>
                                        <div class="col-sm-9">
                                          <p class="form-control-static" id="country-detail-coffeecultivar"></p>
                                        </div>
                                      </div>
                                    </form>							    
								    <p id="country-detail-paragraph"></p>
								                    
				                </div> 
						  		<div class="col-md-6">				                	
								    <?php echo $specificproductionconsumption;?>
				                </div> 
						  	</div>
					    </div>
					</div>
                </div>
                <div class="col-md-3">
                    <h2 style="text-align:center;">World Data</h2>
                </div>
                <div class="col-md-9">
                    <p id="">Select one of the tabs below to view world's coffee production and consumption data.</p>
                </div> 
                     
                <div class="col-md-12">
                    <hr>
                	<div>
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#worldproduction" aria-controls="home" role="tab" data-toggle="tab">Production and Consumption</a></li>
                        <li role="presentation"><a href="#worldconsumptionratio" aria-controls="messages" role="tab" data-toggle="tab">Production Consumption Ratio</a></li>
                        <li role="presentation"><a href="#worldproductiongrowth" aria-controls="profile" role="tab" data-toggle="tab">Production Growth</a></li>
                        <li role="presentation"><a href="#worldconsumptiongrowth" aria-controls="profile" role="tab" data-toggle="tab">Consumption Growth</a></li>
                        <li role="presentation"><a href="#worldproductionshare" aria-controls="messages" role="tab" data-toggle="tab">Production Share</a></li>
                        <li role="presentation"><a href="#worldconsumptionshare" aria-controls="messages" role="tab" data-toggle="tab">Consumption Share</a></li>
                      </ul>
                    
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="worldproduction">
                            <div class="panel panel-default" id="">
                                <div class="panel-body">
                                    <h3>Production and Consumption</h3>
                                    <?php echo $topproducerbar;?>
                                </div>
                            </div>  
                        </div>
                         <div role="tabpanel" class="tab-pane" id="worldconsumptionratio">
                            <div class="panel panel-default" id="">
                                <div class="panel-body">
                                    <h3>Production-Consumption Ratio</h3>
                                    <?php echo $topproducerratiobar;?>
                                </div>
                            </div>  
                        </div>
                        <div role="tabpanel" class="tab-pane" id="worldproductiongrowth">
                            <div class="panel panel-default" id="">
                                <div class="panel-body">
                                    <h3>Production per Year</h3>
                                    <?php echo $topproducerline;?>                          
                                </div>
                            </div>    
                        </div>
                        <div role="tabpanel" class="tab-pane" id="worldconsumptiongrowth">
                            <div class="panel panel-default" id="">
                                <div class="panel-body">
                                    <h3>Consumption per Year</h3>
                                    <?php echo $topconsumerline;?>                          
                                </div>
                            </div>    
                        </div>
                        <div role="tabpanel" class="tab-pane" id="worldproductionshare">
                            <div class="panel panel-default" id="">
                                <div class="panel-body">
                                    <h3>Production Share</h3>
                                    <?php echo $topproducerpie;?>                           
                                </div>
                            </div>  
                        </div>
                        <div role="tabpanel" class="tab-pane" id="worldconsumptionshare">
                            <div class="panel panel-default" id="">
                                <div class="panel-body">
                                    <h3>Consumption Share</h3>
                                    <?php echo $topconsumerpie;?>                           
                                </div>
                            </div>  
                        </div>
                      </div>
                    
                    </div>	
                </div>   
            </div>
        </div>
        
        <!--Footer-->
        <div class="container-fluid" id="footer-container">
            <div class="row">
                <div class="col-md-12">
                    <p>Source: <a href="http://www.ico.org">International Coffee Organization</a></p>
                    <p>This website created for educational purposes.</p>
                </div>
            </div>
        </div>
        
               
    </body>
</html>

<script>
                        var mapboxAccessToken = 'pk.eyJ1IjoibmF1ZmFsZHNyIiwiYSI6ImNqOHp3eHNoejFjMmEzMm5ydmZvcjRudGcifQ.8jKlqdk5Sz8KWxoOhrk_ug';
                        var map = L.map('map', {
                        }).setView([30, 0], 2);                     

                        var southWest = L.latLng(-89.98155760646617, -180),
                        northEast = L.latLng(89.99346179538875, 180);
                        var bounds = L.latLngBounds(southWest, northEast);

                        map.setMaxBounds(bounds);
                        map.on('drag', function() {
                            map.panInsideBounds(bounds, { animate: false });
                        });

                        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + mapboxAccessToken, {
                            id: 'mapbox.light',
                            noWrap: true,
                            minZoom: 2,
                            maxZoom: 3,                         
                        }).addTo(map);                      
                        
                        var show = 'production';
                        
                        function getProducerColor(d){
                            var lightness =  d > 10000 ? 15 :
                                   d > 5000  ? 25 :
                                   d > 2000 ? 35 :
                                   d > 1000  ? 45 :
                                   d > 500   ? 55 :
                                   d > 200   ? 65 :
                                   d > 100  ? 75 :
                                              85;
                                              
                            return 'hsl(216,100%,' + lightness +'%)';
                        }
                                                
                        function getConsumerColor(d){
                            var lightness =  d > 10000 ? 15 :
                                   d > 5000  ? 25 :
                                   d > 2000 ? 35 :
                                   d > 1000  ? 45 :
                                   d > 500   ? 55 :
                                   d > 200   ? 65 :
                                   d > 100  ? 75 :
                                              85;
                                              
                            return 'hsl(24,100%,' + lightness +'%)';
                        }
                        
                        function style(feature) {
                            var fcolor = 'grey';
                            if (show == 'production'){
                                if (feature.properties.production[0] != null){
                                    fcolor = getProducerColor(feature.properties.production[0][1]);
                                }
                            } else {
                                if (feature.properties.consumption[0] != null){
                                    fcolor = getConsumerColor(feature.properties.consumption[0][1]);
                                }
                            }
                            return {
                                fillColor: fcolor,
                                weight: 2,
                                opacity: 1,
                                color: 'white',
                                dashArray: '1',
                                fillOpacity: 0.7
                            };
                        }
                        
                        var info = L.control();

                        info.onAdd = function (map) {
                            this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
                            this.update();
                            return this._div;
                        };
                        
                        // method that we will use to update the control based on feature properties passed
                        info.update = function (props) {
                               if (show == 'production'){
                                        this._div.innerHTML = '<h4>Coffee Production</h4>' +  (props ?
                                        '<b>' + props.name + '</b><br />' + (props.production[0][1]).toLocaleString() + ' thousand 60 kg bag'
                                        : 'Hover over a country');
                                } else {
                                        this._div.innerHTML = '<h4>Coffee Consumption</h4>' +  (props ?
                                        '<b>' + props.name + '</b><br />' + (props.consumption[0][1]).toLocaleString() + ' thousand 60 kg bag'
                                        : 'Hover over a country');
                                } 
                        };
                        
                        info.addTo(map);                                                
                        
                        
                        function highlightFeature(e) {
                            var layer = e.target;
                        
                            layer.setStyle({
                                weight: 5,
                                color: '#666',
                                dashArray: '',
                                fillOpacity: 0.7
                            });
                        
                            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                                layer.bringToFront();
                            }
                            
                            info.update(layer.feature.properties);
                        }
                        
                        function resetHighlight(e) {
                            geojson.resetStyle(e.target);
                            info.update();
                        }
                        
                        function onEachFeature(feature, layer) {
                            layer.on({
                                mouseover: highlightFeature,
                                mouseout: resetHighlight,
                                click: mapClick
                            });                        
                        }
                        
                        function mapClick(e){
                            zoomToFeature(e);
                            updateCountryDetail(e);
                        }
                        
                        function zoomToFeature(e) {
                            map.fitBounds(e.target.getBounds());
                        }
                        
                        document.getElementById("country-detail").style.display = 'none';
                        
                        function updateCountryDetail(e) {
                            //console.log(e['target']['feature']['properties']['name']);
                            var title = document.getElementById("country-detail-title");
                            var flag = document.getElementById("country-detail-flag");
                            var area = document.getElementById("country-detail-area");
                            var population = document.getElementById("country-detail-population");
                            var ngdppc = document.getElementById("country-detail-nominalgdppercapita");
                            var paragraph = document.getElementById("country-detail-paragraph");
                            var coffeecultivar = document.getElementById("country-detail-coffeecultivar");
                            
                            title.innerHTML = e['target']['feature']['properties']['name'];
                            
                            var id = e["target"]["feature"]["id"];
                            flag.innerHTML = "<img src='<?php echo base_url("thirdparty/flagsprites/blank.gif");?>' class='flag flag-" + id.toLowerCase() + "' alt='' />";
                            population.innerHTML = (worldpopulations[id]['population']).toLocaleString() + " (" + worldpopulations[id]['year'] + ")";
                            area.innerHTML = worldareas[id]['area'].toLocaleString() + " km2";
                            ngdppc.innerHTML = "USD " + worldnominalgdppercapitas[id]['gdppercapita'].toLocaleString() + " (" + worldnominalgdppercapitas[id]['source'] + " " + worldnominalgdppercapitas[id]['year'] + ")";
                            paragraph.innerHTML = countrydescription[id]['description'] ? countrydescription[id]['description'] + " <div style='text-align:right'>(Wikipedia)</div>" : '';
                            coffeecultivar.innerHTML = e['target']['feature']['properties']['coffeevar'].length>0 ? e['target']['feature']['properties']['coffeevar'] : "-";

                            
                            document.getElementById("country-detail").style.display = 'inline';
                            document.getElementById("select-country-prompt").style.display = 'none';
                            
                            //setCountryProductionbyID(e['target']['feature']['id']);
                            //
                            setCountryProductionConsumptionbyID(e['target']['feature']['id']);               
                            
                        }
                        
                        
                        var legend2 = L.control({position: 'bottomright'});
                        
                        legend2.onAdd = function (map) {
                        
                            var div = L.DomUtil.create('div', 'info legend'),
                                grades = [0, 100, 200, 500, 1000, 2000, 5000, 10000],
                                labels = [];
                        
                            // loop through our density intervals and generate a label with a colored square for each interval
                            div.innerHTML += '<div>Consumption Quantity</div>';
                            for (var i = 0; i < grades.length; i++) {
                                div.innerHTML +=
                                    '<i style="background:' + getConsumerColor(grades[i] + 1) + '"></i> ' +
                                    grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
                            }
                        
                            return div;
                        };
                        
                        
                        
                        legend2.addTo(map);
                        
                        var legend = L.control({position: 'bottomright'});

                        legend.onAdd = function (map) {
                        
                            var div = L.DomUtil.create('div', 'info legend'),
                                grades = [0, 100, 200, 500, 1000, 2000, 5000, 10000],
                                labels = [];
                        
                            // loop through our density intervals and generate a label with a colored square for each interval
                            div.innerHTML += '<div>Production Quantity</div>';
                            for (var i = 0; i < grades.length; i++) {
                                div.innerHTML +=
                                    '<i style="background:' + getProducerColor(grades[i] + 1) + '"></i> ' +
                                    grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
                            }
                        
                            return div;
                        };
                        
                        legend.addTo(map);
                        
                        var filter = L.control({position: 'bottomleft'});
                        
                        filter.onAdd = function (map) {
                        
                           var div = L.DomUtil.create('div', 'info filter');  
                           
                           var filter_title = document.createElement('h4');
                           filter_title.innerHTML = "Filter: ";
                           
                           var map_form = document.createElement('form');
                           map_form.id = "map_filter";
                           
                           var radio_prod = document.createElement('input');
                           radio_prod.type = 'radio';
                           radio_prod.value = 'production';
                           radio_prod.name = 'map_filter';
                           var att = document.createAttribute("checked");
                           att.value = 'checked';
                           radio_prod.setAttributeNode(att);
                           var radio_prod_label = document.createElement('span');
                           radio_prod_label.innerHTML ="Production";
                           
                           var radio_cons = document.createElement('input');
                           radio_cons.type = 'radio';
                           radio_cons.value = 'consumption';
                           radio_cons.name = 'map_filter';
                           var radio_cons_label = document.createElement('span');
                           radio_cons_label.innerHTML = "Consumption";
                           
                           /*
                           var radio_both = document.createElement('input');
                           radio_both.type = 'radio';
                           radio_both.value = 'both';
                           radio_both.name = 'map_filter';
                           var radio_both_label = document.createElement('span');
                           radio_both_label.innerHTML ="Both";
                           */
                          
                           map_form.appendChild(filter_title);
                           map_form.appendChild(radio_prod);
                           map_form.appendChild(radio_prod_label);
                           map_form.appendChild(radio_cons);
                           map_form.appendChild(radio_cons_label);
                           //map_form.appendChild(radio_both);
                           //map_form.appendChild(radio_both_label);
                           
                           div.appendChild(map_form);
                           
                           map_form.onchange  = function(e){
                                var radios = document.getElementsByName('map_filter');
                                for (var i = 0, length = radios.length; i < length; i++) {
                                    if (radios[i].checked) {
                                        changeMap(radios[i].value);
                                        break;
                                    }
                                }
                           }
                        
                           return div;
                        };
                        
                        filter.addTo(map);
                           
                       legend2.update = function(){
                           if (show == 'production'){
                               this._container.style.display = 'none';
                           } else {
                               this._container.style.display = 'block';
                           }
                           
                       }
                       
                       legend.update = function(){
                           if (show == 'consumption'){
                               this._container.style.display = 'none';
                           } else {
                               this._container.style.display = 'block';
                           }
                       }                          
                       
                       legend.update();
                       legend2.update();
                       
                       function changeMap(val){
                           if (show != val){
                               show = val;
                               legend.update();
                               legend2.update();
                           }
                          map.eachLayer(function(layer){
                              if (layer.feature){
                                    geojson.resetStyle(layer);  
                              }
                                
                          });                     
                           
                       }
                        
                        //var countriesData = [countryData, consumerCountryData];
                        
                        var geojson = L.geoJson(countryData, {
                            style: style,
                            onEachFeature: onEachFeature
                        }).addTo(map);
                        
                    </script>
        
        <style>
            .info {
                padding: 6px 8px;
                font: 14px/16px Arial, Helvetica, sans-serif;
                background: white;
                background: rgba(255,255,255,0.8);
                box-shadow: 0 0 15px rgba(0,0,0,0.2);
                border-radius: 5px;
            }
            .info h4 {
                margin: 0 0 5px;
                color: #777;
            }
            .legend {
                line-height: 18px;
                color: #555;
            }
            .legend i {
                width: 18px;
                height: 18px;
                float: left;
                margin-right: 8px;
                opacity: 0.7;
            }
        </style>