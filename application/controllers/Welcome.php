<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$data['topproducerbar'] = $this->load->view('partial/top_coffee_producer_bar', '', true);
        $data['topproducerratiobar'] = $this->load->view('partial/top_coffee_producer_ratio_bar', '', true);
		$data['topproducerpie'] = $this->load->view('partial/top_coffee_producer_pie', '', true);
        $data['topconsumerpie'] = $this->load->view('partial/top_coffee_consumer_pie', '', true);
		$data['topproducerline'] = $this->load->view('partial/top_coffee_producer_line', '', true);
        $data['topconsumerline'] = $this->load->view('partial/top_coffee_consumer_line', '', true);
        //$data['topproducerlinegrowthpercent'] = $this->load->view('partial/top_coffee_producer_growth_percent_line.php', '', true);
		//$data['specificproducerline'] = $this->load->view('partial/specific_coffee_producer_line', '', true);
        //$data['specificconsumptionline'] = $this->load->view('partial/specific_coffee_consumption_line', '', true);
        $data['specificproductionconsumption'] = $this->load->view('partial/specific_coffee_production_consumption_bar', '', true);
		
		$this->load->view('home', $data);
	}

    public function getfoto(){
        $files = glob("assets/img/coffeeimg/idn/*");
        $i = 0;
        foreach ($files as $src){
            $data[$i] = $src;
            $i++;
        }
        echo json_encode($data);
    }
}
