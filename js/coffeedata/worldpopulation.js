var worldpopulations = {
  "AGO": {
    "population": 19618000,
    "year": 2011
  },
  "BDI": {
    "population": 8575000,
    "year": 2011
  },
  "BOL": {
    "population": 10426154,
    "year": 2010
  },
  "BRA": {
    "population": 210707000,
    "year": 2017
  },
  "CAF": {
    "population": 4487000,
    "year": 2011
  },
  "CIV": {
    "population": 21395000,
    "year": 2009
  },
  "CMR": {
    "population": 19406100,
    "year": 2010
  },
  "COD": {
    "population": 67758000,
    "year": 2011
  },
  "COG": {
    "population": 4140000,
    "year": 2011
  },
  "COL": {
    "population": 49382000,
    "year": 2017
  },
  "CRI": {
    "population": 4563539,
    "year": 2010
  },
  "CUB": {
    "population": 11241161,
    "year": 2010
  },
  "DOM": {
    "population": 9378818,
    "year": 2010
  },
  "ECU": {
    "population": 14483499,
    "year": 2010
  },
  "ETH": {
    "population": 94100000,
    "year": 2011
  },
  "GAB": {
    "population": 1534000,
    "year": 2011
  },
  "GHA": {
    "population": 24233431,
    "year": 2010
  },
  "GIN": {
    "population": 10217591,
    "year": 2009
  },
  "GNQ": {
    "population": 720000,
    "year": 2011
  },
  "GTM": {
    "population": 14713763,
    "year": 2011
  },
  "GUY": {
    "population": 784894,
    "year": 2010
  },
  "HND": {
    "population": 8215313,
    "year": 2011
  },
  "HTI": {
    "population": 10085214,
    "year": 2010
  },
  "IDN": {
    "population": 262571000,
    "year": 2017
  },
  "IND": {
    "population": 1349521845,
    "year": 2017
  },
  "JAM": {
    "population": 2705800,
    "year": 2010
  },
  "KEN": {
    "population": 38610097,
    "year": 2009
  },
  "LAO": {
    "population": 6348800,
    "year": 2011
  },
  "LBR": {
    "population": 3476608,
    "year": 2008
  },
  "LKA": {
    "population": 20653000,
    "year": 2010
  },
  "MDG": {
    "population": 18866000,
    "year": 2008
  },
  "MEX": {
    "population": 122300000,
    "year": 2010
  },
  "MWI": {
    "population": 13077160,
    "year": 2008
  },
  "NIC": {
    "population": 5815524,
    "year": 2010
  },
  "NPL": {
    "population": 26620809,
    "year": 2011
  },
  "PAN": {
    "population": 3405813,
    "year": 2010
  },
  "PER": {
    "population": 29797694,
    "year": 2011
  },
  "PHL": {
    "population": 98390000,
    "year": 2010
  },
  "PNG": {
    "population": 7014000,
    "year": 2011
  },
  "PRY": {
    "population": 6337127,
    "year": 2010
  },
  "RWA": {
    "population": 10412826,
    "year": 2010
  },
  "SLE": {
    "population": 5997000,
    "year": 2011
  },
  "SLV": {
    "population": 6227000,
    "year": 2011
  },
  "TGO": {
    "population": 5753324,
    "year": 2010
  },
  "THA": {
    "population": 69519000,
    "year": 2011
  },
  "TLS": {
    "population": 1066409,
    "year": 2010
  },
  "TTO": {
    "population": 1317714,
    "year": 2010
  },
  "TZA": {
    "population": 43188000,
    "year": 2010
  },
  "UGA": {
    "population": 32939800,
    "year": 2011
  },
  "VEN": {
    "population": 32044000,
    "year": 2017
  },
  "VNM": {
    "population": 89700000,
    "year": 2009
  },
  "YEM": {
    "population": 23833000,
    "year": 2011
  },
  "ZMB": {
    "population": 13046508,
    "year": 2010
  },
  "ZWE": {
    "population": 12754000,
    "year": 2011
  },
  "USA": {
    "population": 324487000,
    "year": 2017
  },
  "CAN": {
    "population": 36155487,
    "year": 2016
  },
  "RUS": {
    "population": 143500000,
    "year": 2017
  },
  "UKR": {
    "population": 45668028,
    "year": 2011
  },
  "TUR": {
    "population": 76667864,
    "year": 2013
  },
  "JPN": {
    "population": 127300000,
    "year": 2017
  },
  "KOR": {
    "population": 48219000,
    "year": 2010
  },
  "TWN": {
    "population": 23197947,
    "year": 2011
  },
  "AUS": {
    "population": 24824091,
    "year": 2017
  },
  "SAU": {
    "population": 27136977,
    "year": 2010
  },
  "DZA": {
    "population": 36300000,
    "year": 2011
  }
};