var worldareas = {
  "AGO": {
    "area": 1246700
  },
  "BDI": {
    "area": 27834
  },
  "BOL": {
    "area": 1098580
  },
  "BRA": {
    "area": 8515767
  },
  "CAF": {
    "area": 622984
  },
  "CIV": {
    "area": 322460
  },
  "CMR": {
    "area": 475440
  },
  "COD": {
    "area": 2345410
  },
  "COG": {
    "area": 342000
  },
  "COL": {
    "area": 1197411
  },
  "CRI": {
    "area": 51100
  },
  "CUB": {
    "area": 109886
  },
  "DOM": {
    "area": 48730
  },
  "ECU": {
    "area": 283560
  },
  "ETH": {
    "area": 1127127
  },
  "GAB": {
    "area": 267667
  },
  "GHA": {
    "area": 238540
  },
  "GIN": {
    "area": 245857
  },
  "GNQ": {
    "area": 28051
  },
  "GTM": {
    "area": 108890
  },
  "GUY": {
    "area": 214970
  },
  "HND": {
    "area": 112090
  },
  "HTI": {
    "area": 27750
  },
  "IDN": {
    "area": 1904556
  },
  "IND": {
    "area": 3287263
  },
  "JAM": {
    "area": 10990
  },
  "KEN": {
    "area": 580367
  },
  "LAO": {
    "area": 236800
  },
  "LBR": {
    "area": 111370
  },
  "LKA": {
    "area": 65611
  },
  "MDG": {
    "area": 587040
  },
  "MEX": {
    "area": 1964375
  },
  "MWI": {
    "area": 118480
  },
  "NIC": {
    "area": 129494
  },
  "NPL": {
    "area": 147181
  },
  "PAN": {
    "area": 78201
  },
  "PER": {
    "area": 1285220
  },
  "PHL": {
    "area": 300000
  },
  "PNG": {
    "area": 462840
  },
  "PRY": {
    "area": 406750
  },
  "RWA": {
    "area": 26338
  },
  "SLE": {
    "area": 72740
  },
  "SLV": {
    "area": 21040
  },
  "TGO": {
    "area": 56785
  },
  "THA": {
    "area": 514000
  },
  "TLS": {
    "area": 14874
  },
  "TTO": {
    "area": 5128
  },
  "TZA": {
    "area": 945087
  },
  "UGA": {
    "area": 241550
  },
  "VEN": {
    "area": 912050
  },
  "VNM": {
    "area": 329560
  },
  "YEM": {
    "area": 527970
  },
  "ZMB": {
    "area": 752614
  },
  "ZWE": {
    "area": 390580
  },
  "USA": {
    "area": 9525067
  },
  "CAN": {
    "area": 9984670
  },
  "RUS": {
    "area": 17098246
  },
  "UKR": {
    "area": 603628
  },
  "TUR": {
    "area": 825418
  },
  "JPN": {
    "area": 377835
  },
  "KOR": {
    "area": 98480
  },
  "TWN": {
    "area": 35980
  },
  "AUS": {
    "area": 7692024
  },
  "SAU": {
    "area": 2149690
  },
  "DZA": {
    "area": 2381740
  }
};