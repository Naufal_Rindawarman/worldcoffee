var worldnominalgdppercapitas = 
  {
  "AGO": {
    "gdppercapita": 3502,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "BDI": {
    "gdppercapita": 325,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "BOL": {
    "gdppercapita": 3197,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "BRA": {
    "gdppercapita": 8727,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "CAF": {
    "gdppercapita": 364,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "CIV": {
    "gdppercapita": 1459,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "CMR": {
    "gdppercapita": 1238,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "COD": {
    "gdppercapita": 495,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "COG": {
    "gdppercapita": 1784,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "COL": {
    "gdppercapita": 5792,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "CRI": {
    "gdppercapita": 11835,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "CUB": {
    "gdppercapita": 7657,
    "source": "United Nations",
    "year": 2015
  },
  "DOM": {
    "gdppercapita": 7159,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "ECU": {
    "gdppercapita": 5930,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "ETH": {
    "gdppercapita": 795,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "GAB": {
    "gdppercapita": 7587,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "GHA": {
    "gdppercapita": 1569,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "GIN": {
    "gdppercapita": 515,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "GNQ": {
    "gdppercapita": 14174,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "GTM": {
    "gdppercapita": 4089,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "GUY": {
    "gdppercapita": 4475,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "HND": {
    "gdppercapita": 2609,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "HTI": {
    "gdppercapita": 761,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "IDN": {
    "gdppercapita": 3604,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "IND": {
    "gdppercapita": 1723,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "JAM": {
    "gdppercapita": 4931,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "KEN": {
    "gdppercapita": 1516,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "LAO": {
    "gdppercapita": 1925,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "LBR": {
    "gdppercapita": 480,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "LKA": {
    "gdppercapita": 3887,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "MDG": {
    "gdppercapita": 391,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "MEX": {
    "gdppercapita": 8555,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "MWI": {
    "gdppercapita": 295,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "NIC": {
    "gdppercapita": 2120,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "NPL": {
    "gdppercapita": 733,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "PAN": {
    "gdppercapita": 13654,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "PER": {
    "gdppercapita": 6199,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "PHL": {
    "gdppercapita": 2924,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "PNG": {
    "gdppercapita": 2528,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "PRY": {
    "gdppercapita": 4003,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "RWA": {
    "gdppercapita": 729,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "SLE": {
    "gdppercapita": 618,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "SLV": {
    "gdppercapita": 4343,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "TGO": {
    "gdppercapita": 590,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "THA": {
    "gdppercapita": 5899,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "TLS": {
    "gdppercapita": 2102,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "TTO": {
    "gdppercapita": 15342,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "TZA": {
    "gdppercapita": 970,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "UGA": {
    "gdppercapita": 638,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "VEN": {
    "gdppercapita": 9258,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "VNM": {
    "gdppercapita": 2173,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "YEM": {
    "gdppercapita": 938,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "ZMB": {
    "gdppercapita": 1275,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "ZWE": {
    "gdppercapita": 977,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "USA": {
    "gdppercapita": 57436,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "CAN": {
    "gdppercapita": 42210,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "RUS": {
    "gdppercapita": 8929,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "UKR": {
    "gdppercapita": 2194,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "TUR": {
    "gdppercapita": 10743,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "JPN": {
    "gdppercapita": 38917,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "KOR": {
    "gdppercapita": 27539,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "TWN": {
    "gdppercapita": 22453,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "AUS": {
    "gdppercapita": 51850,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "SAU": {
    "gdppercapita": 20150,
    "source": "International Monetary Fund",
    "year": 2016
  },
  "DZA": {
    "gdppercapita": 3944,
    "source": "International Monetary Fund",
    "year": 2016
  }
};