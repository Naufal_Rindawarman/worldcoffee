var countrydescription = {
  "AGO": {
    "description": ""
  },
  "BDI": {
    "description": ""
  },
  "BOL": {
    "description": ""
  },
  "BRA": {
    "description": "<p>Coffee production in Brazil is responsible for about a third of all coffee, making Brazil by far the world's largest producer, a position the country has held for the last 150 years. Coffee plantations, covering some 27,000 km2 (10,000 sq mi), are mainly located in the southeastern states of Minas Gerais, São Paulo and Paraná where the environment and climate provide ideal growing conditions.</p><p>The crop first arrived in Brazil in the 18th Century and the country had become the dominant producer by the 1840s. Brazilian coffee prospered since the early 19th century, when the Italian immigrants came to work in the coffee plantations. Production as a share of world coffee output peaked in the 1920's, with the country supplying 100% of the world's coffee, but has declined since the 1950s due to increased global production.</p>"
  },
  "CAF": {
    "description": ""
  },
  "CIV": {
    "description": ""
  },
  "CMR": {
    "description": ""
  },
  "COD": {
    "description": ""
  },
  "COG": {
    "description": ""
  },
  "COL": {
    "description": "<p>The coffee plant had spread to Colombia by 1790.[12] The oldest written testimony of the presence of coffee in Colombia is attributed to a Jesuit priest, José Gumilla. In his book The Orinoco Illustrated (1730), he registered the presence of coffee in the mission of Saint Teresa of Tabajé, near where the Meta river empties into the Orinoco. Further testimony comes from the archbishop-viceroy Caballero y Gongora (1787) who registered the presence of the crop in the north east of the country near Giron (Santander) and Muzo (Boyaca) in a report that he provided to the Spanish authorities.</p>\n\n<p>The first coffee crops were planted in the eastern part of the country. In 1808 the first commercial production was registered with 100 green coffee bags (60 kg each) that were exported from the port of Cucuta, near the border with Venezuela. A priest named Francisco Romero is attributed to have been very influential in the propagation of the crop in the northeast region of the country. After hearing the confession of the parishioners of the town of Salazar de la Palmas, he required as penance the cultivation of coffee. Coffee became established in the departments of Santander and North Santander, Cundinamarca, Antioquia, and the historic region of Caldas.</p>"
  },
  "CRI": {
    "description": "<p>Coffee was first introduced in the Dominican Republic in 1715 and has been the principal crop of the small scale farmers. Coffee began to be exported circa 1872. In the early 20th century, the crop was cultivated in all the Cibao, principally in the district of Puerto Plata. The exportation of coffee from the Dominican Republic in 1900 amounted to 3,951,539 pounds (1,792,388 kg). Important coffee areas in 1918 were in Moca, Santiago and Baní, with approximately 66% of the crop exported from Puerto Plata.</p>\n\n<p>The area under coffee plantation was 120,000 hectares (300,000 acres) (about 3% of land under farming), but since 1981 the area cropped has substantially declined, but the production level has remained generally the same due to the adoption of modern technological inputs. There are five major coffee producing regions, four of them being in the hilly region – the Central Mountain Region, the Northern Mountain Region, the Neyba Mountain Range, and the Bahoruco Mountain Range. There were 40,000 to 50,000 farmers operating in this sector. </p>"
  },
  "CUB": {
    "description": ""
  },
  "DOM": {
    "description": ""
  },
  "ECU": {
    "description": ""
  },
  "ETH": {
    "description": "<p>Ethiopia is the world's seventh largest producer of coffee, and Africa's top producer, with 260,000 metric tonnes in 2006. Half of the coffee is consumed by Ethiopians, and the country leads the continent in domestic consumption. The major markets for Ethiopian coffee are the EU (about half of exports), East Asia (about a quarter) and North America. The total area used for coffee cultivation is estimated to be about 4,000 km2 (1,500 sq mi), the size is unknown due to the fragmented nature of the coffee farms. The way of production has not changed much, with nearly all work, cultivating and drying, still done by hand.</p>\n\n<p>The revenues from coffee exports account for 10% of the annual government revenue, because of the large share the industry is given very high priority, but there are conscious efforts by the government to reduce the coffee industry's share of the GDP by increasing the manufacturing sector.</p>\n\n<p>The Coffee and Tea Authority, part of the federal government, handles anything related to coffee and tea, such as fixing the price at which the washing stations buy coffee from the farmers. This is a legacy from a nationalization scheme set in action by the previous regime that turned over all the washing stations to farmers cooperatives. The domestic market is heavily regulated through licenses, with the goal of avoiding market concentration.</p>"
  },
  "GAB": {
    "description": ""
  },
  "GHA": {
    "description": ""
  },
  "GIN": {
    "description": ""
  },
  "GNQ": {
    "description": ""
  },
  "GTM": {
    "description": "<p>The coffee industry began to develop in Guatemala in the 1850s and 1860s, initially mixing its cultivation with cochineal. Small plantations flourished in Amatitlán and Antigua areas in the southwest.[4] Initial growth though was slow due to lack of knowledge and technology. Many planters had to rely on loans and borrow from their families to finance their coffee estates (fincas) with coffee production in Guatemala increasingly owned by foreign companies who possessed the financial power to buy plantations and provide investment.[4]</p>\n\n<p>A scarcity of laborers was the main obstacle to a rapid increase of coffee production in Guatemala. In 1887, the production was over 22,000,000 kg (48,500,000 lb). In 1891, it was over 24,000,000 kg (52,000,000 lb). From 1879 to 1883, Guatemala exported 133,027,289 kg (293,274,971 lb) pounds of coffee. By 1902 the most important coffee plantations were found on the southern coast.</p>\n\n<p>Many acres of land were suitable for this cultivation, and the varieties that were produced in the temperate regions were superior. Coffee was grown around Guatemala City, Chimaltenango, and Verapaz. The majority of the plantations were located in the departments of Guatemala, Amatitlan, Sacatepequez, Solola, Retalhuleu, Quezaltenango, San Marcos, and Alta Verapaz.</p>"
  },
  "GUY": {
    "description": ""
  },
  "HND": {
    "description": "<p>The cultivation of the coffee plant was in its infancy in the Republic of Honduras at the end of the 19th century. While there were numerous coffee plantations at the time, they were small. The soil, climate, and conditions in Honduras are the same as those of Guatemala, Nicaragua, or Costa Rica. The drawback in Honduras was lack of means of transportation and facilities for shipment to the coast. There was practically no exportation of coffee from Honduras, the product was mostly sold domestically. A new plantation of coffee would begin to produce a profit by the end of the fourth year after planting, and after the seventh year a profit of from 100 to 300 per cent on the capital invested could be expected. The production of coffee in 1894 was reckoned at 20,000 quintals, of which only 10 per cent was exported. The exportation was from the Salvadoran ports Amapala and Puerto Cortes. In 1900, Honduras exported 54,510 pesos worth of coffee.</p>"
  },
  "HTI": {
    "description": ""
  },
  "IDN": {
    "description": "<p>Indonesia was the fourth largest producer of coffee in the world in 2014. Coffee in Indonesia began with its colonial history, and has played an important part in the growth of the country. Indonesia is located within an ideal geography for coffee plantations, near the equator and with numerous mountainous regions across the islands, creating well suited micro-climates for the growth and production of coffee.</p>\n\n<p>Indonesia produced an estimated 540,000 metric tons of coffee in 2014. Of this total, it is estimated that 154,800 tons was required for domestic consumption in the 2013/2014 financial year. Of the exports, 25% are arabica beans; the balance is robusta. In general, Indonesia’s arabica coffees have low acidity and strong body, which makes them ideal for blending with higher acidity coffees from Central America and East Africa.</p>"
  },
  "IND": {
    "description": "<p>offee production in India is dominated in the hill tracts of South Indian states, with the state of Karnataka accounting 71% followed by Kerala 21% and Tamil Nadu 5% of production of 8,200 tonnes. Indian coffee is said to be the finest coffee grown in the shade rather than direct sunlight anywhere in the world. There are approximately 250,000 coffee growers in India; 98% of them are small growers. As of 2009, the production of coffee in India was only 4.5% of the total production in the world. Almost 80% of the country's coffee production is exported. Of that which is exported, 70% is bound for Germany, Russian federation, Spain, Belgium, Slovenia, United States, Japan, Greece, Netherlands and France, and Italy accounts for 29% of the exports. Most of the export is shipped through the Suez Canal.</p>\n\n<p>Coffee is grown in three regions of India with Karnataka, Kerala and Tamil Nadu forming the traditional coffee growing region of India, followed by the new areas developed in the non-traditional areas of Andhra Pradesh and Orissa in the eastern coast of the country and with a third region comprising the states of Assam, Manipur, Meghalaya, Mizoram, Tripura, Nagaland and Arunachal Pradesh of Northeastern India, popularly known as “Seven Sister States of India\".</p>"
  },
  "JAM": {
    "description": ""
  },
  "KEN": {
    "description": ""
  },
  "LAO": {
    "description": ""
  },
  "LBR": {
    "description": ""
  },
  "LKA": {
    "description": ""
  },
  "MDG": {
    "description": ""
  },
  "MEX": {
    "description": ""
  },
  "MWI": {
    "description": ""
  },
  "NIC": {
    "description": ""
  },
  "NPL": {
    "description": ""
  },
  "PAN": {
    "description": ""
  },
  "PER": {
    "description": "<p>In 1895, the Journal of the Society of Arts recorded that Peru was known for many years as a coffee-producing country, but the coffee grown on the coast was used primarily for domestic consumption, and it was only later that it developed as an exporting nation. Coffee planting began, and coffee is still cultivated near the port of Pacasmayo. Coffee has been cultivated in the south, in the districts of Sandia and Carabaya, and in the centre of Peru in the valleys of Chanchamayu, Viloc, and Huánuco. Production in Chanchamayo district was facilitated by the completion of the Central (or Oroya Railway) by the Peruvian Corporation. The Chanchamayu Valley, itself about 10 miles (16 km) long, was in the hands of private plantation owners, while the Perené, Paucartambo, and Rio Colorado valleys, were later linked by railway.</p>"
  },
  "PHL": {
    "description": ""
  },
  "PNG": {
    "description": ""
  },
  "PRY": {
    "description": ""
  },
  "RWA": {
    "description": ""
  },
  "SLE": {
    "description": ""
  },
  "SLV": {
    "description": ""
  },
  "TGO": {
    "description": ""
  },
  "THA": {
    "description": ""
  },
  "TLS": {
    "description": ""
  },
  "TTO": {
    "description": ""
  },
  "TZA": {
    "description": ""
  },
  "UGA": {
    "description": ""
  },
  "VEN": {
    "description": ""
  },
  "VNM": {
    "description": "<p>Coffee was introduced to Vietnam in 1857 by the French and slowly grew as producer of coffee in Asia. The height of coffee production occurred in the early 20th century as small-scale production shifted towards plantations. The first instant coffee plant, Coronel Coffee Plant, was established in Biên Hòa, ??ng Nai Province in 1969, with a production capacity of 80 tons per year.</p>"
  },
  "YEM": {
    "description": ""
  },
  "ZMB": {
    "description": ""
  },
  "ZWE": {
    "description": ""
  },
  "USA": {
    "description": ""
  },
  "CAN": {
    "description": ""
  },
  "RUS": {
    "description": ""
  },
  "UKR": {
    "description": ""
  },
  "TUR": {
    "description": ""
  },
  "JPN": {
    "description": ""
  },
  "KOR": {
    "description": ""
  },
  "TWN": {
    "description": ""
  },
  "AUS": {
    "description": ""
  },
  "SAU": {
    "description": ""
  },
  "DZA": {
    "description": ""
  }
};